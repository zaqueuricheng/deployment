$PROJECT=(gcloud config get-value project)

gcloud projects list --filter="$PROJECT" --format="value(PROJECT_NUMBER)"

gcloud projects list

gcloud projects get-iam-policy monitoramento-valid --flatten="bindings[].members" --format='table(bindings.role)' --filter="bindings.members:service-638833707982@containerregistry.iam.gserviceaccount.com"

gcloud projects add-iam-policy-binding monitoramento-valid --member=serviceAccount:service-638833707982@containerregistry.iam.gserviceaccount.com --role=roles/containerregistry.ServiceAgent
## Start cluster and stop cluster when works test finished 
- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

## Create variables to access cluster for pipeline
## 1 - Create service account (e-mail) - sign2go-business-registry, sign2go-business-dev, sign2go-business-prod, sign2go-business-stagging
- gcloud iam service-accounts create dev-sign2go-business
- gcloud iam service-accounts list | Select-String -Pattern 'dev-sign2go-business'
## 2 - Create credentials
- $SERVICE_ACCOUNT_EMAIL="dev-sign2go-business@monitoramento-valid.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create credentials-sign2go-business-dev --iam-account $SERVICE_ACCOUNT_EMAIL
## Create variables on pipeline files graphical environment
- AUTH_JSON_KUBE_DEV

## 3 - Application rules on service account
## 3.1 - Go to Service Account and Copy svc (e-mail)
- dev-sign2go-business@monitoramento-valid.iam.gserviceaccount.com
## 3.2 - Go to IAM and past svc (e-mail)
## 3.2.1 - Add specific rules to your IAM 
- Kubernetes Engine Admin

## Deployment
- kubectl create ns sign2go-business-api
- kubectl apply -f deployment.yaml -n sign2go-business-api

- kubectl get deployment -n sign2go-business-api
- kubectl get svc -n sign2go-business-api
- kubectl port-forward kong-review-kong-d9dbd5d84-qc4zx 8001:80

## Create ns and run pipeline

- AUTH_JSON_REGISTRY - credentials of gcloud registry/sign2go-dev

- AUTH_JSON_KUBE_DEV - credentials of gcloud dev project/sign2go-dev
- AUTH_JSON_KUBE_HML - credentials of gcloud hml project/sign2go-hml
- AUTH_JSON_KUBE_PRD - credentials of gcloud prod project/sign2go-prod


- gcloud auth activate-service-account --project=sign2go-hml --key-file=autenticacao.json
- gcloud auth activate-service-account --key-file=autenticacao.json
- gcloud container clusters get-credentials sign2go --region southamerica-east1 --project sign2go-hml

- a url definidas:
    - api.sign2go.com.br
    - dev-api.sign2go.com.br
    - hml-api.sign2go.com.br

- kubectl logs sign2go-business-api-cd68f977b-bch46 -n sign2go-business-api

## Test
- kubectl port-forward sign2go-business-api-678bb58fcf-ll8n9 80:80 -n sign2go-business-api
- http://localhost/api/health

## Ingress
- kubectl apply -f business-ingress-dev.yaml -n sign2go-business-api

## Service
- kubectl apply -f business-svc-dev.yaml -n sign2go-business-api
- kubectl get deploy,pod,svc,ing -n sign2go-business-api
- kubectl get services -n ingress-nginx


## 
- // https://sign2go.com.br/api/health


##
- kubectl apply -f business-deploy-dev.yaml -n sign2go-business-api
- kubectl apply -f business-deploy-hml.yaml -n sign2go-business-api
- kubectl apply -f business-deploy-prod.yaml -n sign2go-business-api