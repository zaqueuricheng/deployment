# Create credentials for GCP Cloud connect

# Create IAM for GCP
- $PROJECT_ID=gcloud config get-value project # Get gcp project ID
- gcloud iam service-accounts create sign2go-business # Create service account for bar service
- gcloud iam service-accounts list # List all service account
- gcloud iam service-accounts list | Select-String -Pattern 'sign2go-business' # List specific service account
- $SERVICE_ACCOUNT_EMAIL=gcloud iam service-accounts list --filter sign2go-business@monitoramento-valid.iam.gserviceaccount.com

# Create Role and mapping permissions on the bucket
# https://cloud.google.com/iam/docs/creating-custom-roles
- gcloud iam roles --help
- gcloud iam roles list
- gcloud iam roles list --project=$PROJECT_ID

- $ROLE_ID="sign2go_business"
- gcloud iam roles create $ROLE_ID --project=$PROJECT_ID --file=C:\workspaces\sign2go-business-api\auto-deployment-values\credentials\iam-roles.yaml   
- gcloud iam roles describe $ROLE_ID --project=$PROJECT_ID
- gcloud iam roles delete $ROLE_ID --project=$PROJECT_ID

# Create key credentials acess - To see on gcp cloud console project/<IAM & Admin>/<Service Account>
- gcloud iam service-accounts keys create credentials-sign2go-business --iam-account $SERVICE_ACCOUNT_EMAIL
