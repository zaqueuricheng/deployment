## Start cluster and stop cluster when works test finished 
- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

## Create namespace
- kubectl create ns sign2go-business-api
## Deployment your application
- kubectl apply -f business-deployment.yaml -n sign2go-business-api

## Create variables to access cluster for pipeline
## 1 - Create service account (e-mail) - sign2go-business-registry, sign2go-business-dev, sign2go-business-hml, sign2go-business-prod
- gcloud iam service-accounts create sign2go-business-prod
- gcloud iam service-accounts list | Select-String -Pattern 'sign2go-business-prod'
## 2 - Create credentials
- $SERVICE_ACCOUNT_EMAIL="sign2go-business-prod@sign2go-prd.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create credentials-sign2go-business-prod --iam-account $SERVICE_ACCOUNT_EMAIL
## Create variables on pipeline files graphical environment
- AUTH_JSON_REGISTRY - credentials of gcloud registry/sign2go-dev

- AUTH_JSON_KUBE_DEV - credentials of gcloud dev project/sign2go-dev
- AUTH_JSON_KUBE_HML - credentials of gcloud hml project/sign2go-hml
- AUTH_JSON_KUBE_PRD - credentials of gcloud prod project/sign2go-prod

## 3 - Application rules on service account
## 3.1 - Go to Service Account and Copy svc (e-mail)
- sign2go-business-prod@sign2go-prd.iam.gserviceaccount.com
## 3.2 - Go to IAM and past svc (e-mail)
## 3.2.1 - Add specific rules to your IAM
# AUTH_JSON_REGISTRY 
- Container Registry Service Agent
- Storage Admin
# AUTH_JSON_KUBE_DEV
- Kubernetes Engine Admin

## Test

- kubectl logs sign2go-business-api-cd68f977b-bch46 -n sign2go-business-api
- kubectl get deployment -n sign2go-business-api
- kubectl get svc -n sign2go-business-api

- kubectl port-forward sign2go-business-api-6d4d54f7b7-xnkxk 80:80 -n sign2go-business-api
- http://localhost/api/health

## DNS
- a url definidas:
    - business.sign2go.com.br/api/health
    - business-dev.sign2go.com.br/api/health
    - business-hml.sign2go.com.br/api/health

## Create data-base
- C:\sign2go-business-api\auto-deploy-values\credentials\data-bases\README.md

kubectl port-forward sign2go-business-api-8555c8fd76-2gwhb 80:80 -n sign2go-business-api