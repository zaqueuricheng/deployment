- gcloud projects list
- gcloud config set project sign2go-dev
- gcloud config get-value project

## 1 - Create service account (e-mail) - sign2go-business-registry, sign2go-business-dev, sign2go-business-hml, sign2go-business-prod
- Nota: O service account para acessar o registry deve ser criado no projeto onde os containers estarão armazenados.
- gcloud iam service-accounts create sign2go-business-pull-image
- gcloud iam service-accounts list | Select-String -Pattern 'sign2go-business-pull-image'
## 2 - Create credentials
- Nota: Executar o comando no diretório onde estará o arquivo com as credencias
- $SERVICE_ACCOUNT_EMAIL="sign2go-business-pull-image@sign2go-dev.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create business-pull-image --iam-account $SERVICE_ACCOUNT_EMAIL

## 3 - Criar o secret
- Nota: Executar o comando no linux, no windows deu erro. Pode usar o wsl2! Devera executar no ns padrão e ns onde o svc será deployado
- kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat sign2go-dev-secret.json)" --docker-email="pull-image-gcr-kube@sign2go-dev.iam.gserviceaccount.com" -n sign2go-business-api




kubectl create secret docker-registry my-secret --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat sign2go-dev-0c04ef3be032.json)" --docker-email="pull-image-gcr-kube@sign2go-dev.iam.gserviceaccount.com"

kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gcr-json-key"}]}'