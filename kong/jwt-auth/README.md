## AUTENTICAÇÃO/Segurança

# ACL (Access Control List) - Contains rules that grant or deny access to certain digital environments

# TLS (Transport Layer Security) - 

# JWT (Json Web Token) - Is an open standard that defines a compact and self-contained way for securely transmitting information between parties as a JSON object.

## Cluster
- gcloud projects list
- gcloud config set project <PROJECT_ID>

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type n2-standard-2

# Deployment and Service
- kubectl apply -f https://bit.ly/k8s-httpbin -n kong 

# Test in localhost
- kubectl get pods,deploy,svc,ing,secret,pvc -n kong
- kubectl port-forward httpbin-5b55598456-f7qbk 80:80 -n kong

# Create an ingress for this service
- kubectl apply -f get-ingress.yaml -n kong
- kubectl apply -f post-ingress.yaml -n kong

# Test the Ingress rules
- curl -i 35.223.220.213/get
- curl -i --data "foo=bar" -X POST 35.223.220.213/post

# Add JWT authentication plugin to the service
kubectl apply -f auth.yaml -n kong

# Associate the plugin to the Ingress rules we created earlier
- https://docs.konghq.com/hub/kong-inc/jwt/

- Edit the line
    annotations:
        konghq.com/plugins: app-jwt

- kubectl apply -f get-ingress.yaml -n kong
- kubectl apply -f post-ingress.yaml -n kong

# Test again and the both request is not authorized
- curl -i 35.223.220.213/get
- curl -i --data "foo=bar" -X POST 35.223.220.213/post

# Provision Consumers
- kubectl apply -f consumers-plain.yaml -n kong
- kubectl apply -f consumers-admin.yaml -n kong

# Create secret
- kubectl create secret generic app-admin-jwt --from-literal=kongCredType=jwt --from-literal=key="admin-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY-----MIIBIjA....-----END PUBLIC KEY-----" -n kong

- kubectl create secret generic app-user-jwt --from-literal=kongCredType=jwt --from-literal=key="user-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY-----qwerlkjqer....-----END PUBLIC KEY-----" -n kong

# Update the consumers to use the credentials that we created
- kubectl apply -f consumers-plain.yaml -n kong
- kubectl apply -f consumers-admin.yaml -n kong

# Test
- kubectl get pods,deploy,svc,ing,secret,pvc,kongconsumer -n kong

- kubectl get secret app-admin-jwt -n kong -o yaml

- curl -i 35.223.220.213/get
- curl -i -H "Authorization: Bearer ${USER_JWT}" 35.223.220.213/get

- curl -i -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRYTmxjaTFwYzNOMVpYST0ifQ.eyJuYW1lIjoicGxhaW4tdXNlciJ9.C8n0fX6772IqNjPPGH6a8uqRzqWcPhYN-NTvLzjJP24" 35.223.220.213/get


### New kong auth test
## Install ingress kong
- https://docs.konghq.com/kubernetes-ingress-controller/1.1.x/deployment/gke/
# Update User Permissions
- kubectl apply -f rolebinding.yaml -n kube-system

# Deploy the Kubernetes Ingress Controller
- kubectl create -f https://bit.ly/k4k8s
- export PROXY_IP=$(kubectl get -o jsonpath="{.status.loadBalancer.ingress[0].ip}" service -n kong kong-proxy)
- $PROXY_IP

## Testing Connectivity to Kong
https://docs.konghq.com/kubernetes-ingress-controller/1.1.x/guides/configure-acl-plugin/?_ga=2.227574109.194714481.1620061771-177187029.1620061771#use-the-credential

# Test 
- curl -i $PROXY_IP

# Setup a Sample Service
- kubectl apply -f https://bit.ly/k8s-httpbin

# Create two Ingress rules to proxy the httpbin service we just created
- kubectl apply -f ingress-get.yaml
- kubectl apply -f ingress-post.yaml

# Test the Ingress rules (Return 200 Works!)
- curl -i $PROXY_IP/get
- curl -i --data "foo=bar" -X POST $PROXY_IP/post

# Add JWT authentication to the service
- kubectl apply -f authentication.yaml

# Associate the plugin to the Ingress rules we created earlier
# Update files and apply
- kubectl apply -f ingress-get.yaml
- kubectl apply -f ingress-post.yaml

# Test the Ingress rules (Return 401 not Works!)
- curl -i $PROXY_IP/get
- curl -i --data "foo=bar" -X POST $PROXY_IP/post

# Provision Consumers
kubectl apply -f consumers-admin.yaml
kubectl apply -f consumers-plain-user.yaml

## Secrets
# JWT signing key
kubectl create secret generic app-admin-jwt --from-literal=kongCredType=jwt --from-literal=key="admin-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY----- MIIBIjA.... -----END PUBLIC KEY-----"

kubectl create secret generic app-user-jwt --from-literal=kongCredType=jwt --from-literal=key="user-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY----- qwerlkjqer.... -----END PUBLIC KEY-----"

# Update Consumers
# Edit consumers file and apply
kubectl apply -f consumers-admin.yaml
kubectl apply -f consumers-plain-user.yaml

## 
export USER_JWT="plain-user"
export ADMIN_JWT="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRYTmxjaTFwYzNOMVpYST0ifQ"

curl -i -H "Authorization: Bearer dXNlci1pc3N1ZXI=}" $PROXY_IP/get
curl -i -H "Authorization: Bearer YWRtaW4taXNzdWVy}" $PROXY_IP/get