- https://docs.konghq.com/gateway-oss/2.4.x/kong-for-kubernetes/install/

## Kong

# Install
- helm repo list
- helm repo add kong https://charts.konghq.com
- helm repo update
- helm inspect values kong/kong > values.yaml
- helm search repo kong

# kong/kong
- helm install kong-review --namespace=default -f values.yaml kong/kong --version 2.1.0

# bitnami/kong
- helm install kong-review --namespace=default -f values.yaml bitnami/kong --version 3.6.0

#
- https://learnk8s.io/kubernetes-ingress-api-gateway#:~:text=Kong%20is%20an%20API%20gateway,packaged%20as%20a%20Kubernetes%20Ingress.


## Istio as an API Gateway


https://javascript.plainenglish.io/deploying-a-localhost-server-with-node-js-and-express-js-58775f098407

https://mherman.org/blog/dockerizing-a-react-app/