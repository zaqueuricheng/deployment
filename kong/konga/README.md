## My Cluster
- gcloud projects list
- gcloud config set project <PROJECT_ID>

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type n2-standard-2

# Install kong chart
- helm repo list
- helm repo add kong https://charts.konghq.com
- helm repo update
## postgresql
- helm search repo postgres
- helm inspect values bitnami/postgresql > values.yaml
- helm install postgresql-review --namespace=default -f postgresql.yaml bitnami/postgresql --version 10.4.3
- helm upgrade postgresql-review --namespace=default -f postgresql.yaml bitnami/postgresql --version 10.4.3

## myqsl
- helm search repo mysql
- helm inspect values bitnami/mysql > values.yaml
- helm install mysql-review --namespace=default -f mysql.yaml bitnami/mysql --version 8.5.7

## kong/kong
- helm search repo kong
- helm inspect values kong/kong > values.yaml
- helm install kong-review --namespace=default -f values.yaml kong/kong --version 2.1.0
### bitnami/kong
- helm install kong-review --namespace=default -f values.yaml bitnami/kong --version 3.6.0

## konga
- https://medium.com/@tselentispanagis/managing-microservices-and-apis-with-kong-and-konga-7d14568bb59d
- https://pantsel.github.io/konga/
- https://www.youtube.com/watch?v=_2GRXgYswhI

- https://dev.to/vousmeevoyez/setup-kong-konga-part-2-dan#add-konga-to-docker-compose

- https://faun.pub/kong-api-gateway-with-konga-dashboard-ae95b6d1fec7

- helm install kong-review --namespace=default -f kong.yaml bitnami/kong --version 3.6.0

### docker compose konga