## IMAGES
https://www.youtube.com/watch?v=DwlIn9zOcfc


https://stackoverflow.com/questions/35266769/how-to-list-the-published-container-images-in-the-google-container-registry-usin


## Steps
https://cloud.google.com/kubernetes-engine/docs/quickstarts/deploying-a-language-specific-app?hl=pt-br#standard
# 1 - package.json

# 2 - index.js

# 3 - Dockerfile

# 4 - .dockerignore

# 5 - Cria uma imagem

- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

- gcloud builds submit --tag gcr.io/project-id/helloworld-gke .
- gcloud builds submit --tag gcr.io/monitoramento-valid/helloworld-nodejs-gke .

# 6 - Implantar o aplicativo
- objeto de implantação
    - deployment.yaml
    - kubectl create ns my-applications
    - kubectl apply -f deployment.yaml -n my-applications
    - kubectl get deployments

- objeto service
    - service.yaml
    - kubectl apply -f service.yaml -n my-applications
    - kubectl get services -n my-applications
    - curl 35.238.229.59 

# Realizar limpeza para evitar cobranças
- Excluir o projeto a partir do console do google
- Excluir o cluster e o contêiner
    -  gcloud container clusters delete $CLUSTER_NAME --region $CLUSTER_REGION
    -  gcloud container images delete $IMAGEM


## New
# List images
- gcloud container images list

# Delete images
- gcloud container images delete gcr.io/monitoramento-valid/bar
- gcloud container images delete gcr.io/monitoramento-valid/foo

- gcloud builds submit --tag gcr.io/monitoramento-valid/bar:0.0.2 . // Copy the image...

## Deployment
- kubectl apply -f bar.yaml -n my-applications

## Service
- kubectl apply -f service.yaml -n my-applications

## Teste
- kubectl port-forward bar-deployment-6f5d777956-mspjv 80:8080

## After install kong
- kubectl apply -f bar-ingress.yaml -n my-applications

## New
## Create image

## Create deploy and service
kubectl apply -f foo.yaml -n kong

# Test if the svc is normal

# Install kong
- helm install kong/kong --generate-name --set ingressController.installCRDs=false -n kong

# Create Ingress
- kubectl apply -f bar-ingress.yaml -n kong




# Create secret
- kubectl get secret -n sign2go-business-dev
- kubectl apply -f db-secret-dev.yaml

# Edit deployment file and apply
- kubectl apply -f business-deploy-dev.yaml -n sign2go-business-dev

# Ingress
- kubectl apply -f business-ingress-dev.yaml -n sign2go-business-dev
- C:\Windows\System32\drivers\etc //

- kubectl create secret tls sign2go-cert-tls --cert=star.sign2go.com.br.bundle.crt --key=star.sign2go.com.br.key -n sign2go-business-dev

# ############################################################################### #

## Start cluster and stop cluster when works test finished 
- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

## Create namespace
- kubectl create ns sign2go-business-dev
## Deployment your application
- kubectl apply -f business-deployment.yaml -n sign2go-business-dev

## Create variables to access cluster for pipeline
## 1 - Create service account (e-mail) - sign2go-business-registry, sign2go-business-dev, sign2go-business-hml, sign2go-business-prod
- gcloud iam service-accounts create sign2go-business-prod
- gcloud iam service-accounts list | Select-String -Pattern 'sign2go-business-prod'
## 2 - Create credentials
- $SERVICE_ACCOUNT_EMAIL="sign2go-business-prod@sign2go-prd.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create credentials-sign2go-business-prod --iam-account $SERVICE_ACCOUNT_EMAIL
## Create variables on pipeline files graphical environment
- AUTH_JSON_REGISTRY - credentials of gcloud registry/sign2go-dev

- AUTH_JSON_KUBE_DEV - credentials of gcloud dev project/sign2go-dev
- AUTH_JSON_KUBE_HML - credentials of gcloud hml project/sign2go-hml
- AUTH_JSON_KUBE_PRD - credentials of gcloud prod project/sign2go-prod

## 3 - Application rules on service account
## 3.1 - Go to Service Account and Copy svc (e-mail)
- sign2go-business-prod@sign2go-prd.iam.gserviceaccount.com
## 3.2 - Go to IAM and past svc (e-mail)
## 3.2.1 - Add specific rules to your IAM
# AUTH_JSON_REGISTRY 
- Container Registry Service Agent
- Storage Admin
# AUTH_JSON_KUBE_DEV
- Kubernetes Engine Admin

## Test

- kubectl logs sign2go-business-dev-cd68f977b-bch46 -n sign2go-business-dev
- kubectl get deployment -n sign2go-business-dev
- kubectl get svc -n sign2go-business-dev

- kubectl port-forward sign2go-business-dev-6d4d54f7b7-xnkxk 80:80 -n sign2go-business-dev
- http://localhost/api/health

## dns
- a url definidas:
    - business.sign2go.com.br/api/health
    - business-dev.sign2go.com.br/api/health
    - business-hml.sign2go.com.br/api/health

## Create data-base
- C:\sign2go-business-dev\auto-deploy-values\credentials\data-bases\README.md

kubectl port-forward sign2go-business-dev-8555c8fd76-2gwhb 80:80 -n sign2go-business-dev

## 
# - ingress
- kubectl apply -f business-ingress-dev.yaml -n sign2go-business-dev
- C:\Windows\System32\drivers\etc //

- kubectl create secret tls sign2go-cert-tls --cert=star.sign2go.com.br.bundle.crt --key=star.sign2go.com.br.key -n sign2go-business-dev

## - hml
- kubectl create ns sign2go-business-hml
- kubectl apply -f db-secret-hml.yaml

# - Executar esses comando no linux, faça no ns padrão no ns onde ficará o deployment
- cat sign2go-dev-secret.json 
- kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat sign2go-dev-secret.json)" --docker-email="pull-image-gcr-kube@sign2go-dev.iam.gserviceaccount.com" -n sign2go-business-hml
- kubectl apply -f business-deploy-hml.yaml
- kubectl apply -f business-svc-hml.yaml
- kubectl apply -f .\business-ingress-hml.yaml
- Test // Não carregará a página, para resolver coloquei o IP e o DNS no arquivo host
- Test // Carregará mas vai solicitar o certificado resolve com a linha abaixo
- kubectl create secret tls sign2go-cert-tls --cert=star.sign2go.com.br.bundle.crt --key=star.sign2go.com.br.key -n sign2go-business-hml

## - prod
- kubectl create ns sign2go-business-prd
- kubectl apply -f db-secret-prod.yaml
- kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-pass
word="$(cat sign2go-dev-secret.json)" --docker-email="pull-image-gcr-kube@sign2go-dev.iam.gserviceaccount.com" -n sign2go-business-prd
- kubectl apply -f .\business-deploy-prod.yaml
- kubectl apply -f .\business-svc-prod.yaml
- kubectl apply -f .\business-ingress-prod.yaml

## Teste DNS local
- Open nodepad with terminal
- Open host file
    - C:\Windows\System32\drivers\etc\host
- Edit host file add the line billow in End of section
    - 35.199.79.254 dev-business.sign2go.com.br
    - 35.247.193.93 hml-business.sign2go.com.br
    - 35.199.72.127 business.sign2go.com.br

## Add certificate for https
- kubectl create secret tls sign2go-cert-tls --cert=star.sign2go.com.br.bundle.crt --key=star.sign2go.com.br.key -n sign2go-business-prd

# Add host on GCP DNS -> Network services -> Add record set
- 35.199.79.254 dev-business.sign2go.com.br
- 35.247.193.93 hml-business.sign2go.com.br
- 35.199.72.127 business.sign2go.com.br

https://dev-business.sign2go.com.br/api/health
https://hml-business.sign2go.com.br/api/health
https://business.sign2go.com.br/api/health

kubectl apply -f business-deploy-dev.yaml
kubectl apply -f business-deploy-hml.yaml
kubectl apply -f business-deploy-prod.yaml




### 
aHoDOgcE5onOAxA3
YUhvRE9nY0U1b25PQXhBMw==


postgres