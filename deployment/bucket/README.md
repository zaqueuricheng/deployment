## Acesso aos bucket

1 - Criar service account (Console)
    NOME: prd-business-bucket DESCRIÇÃO: service account for business bucket

2 - Add sa to IAM (Console)
    PERMISSIONS: Compute Storage Admin
                 Storage Admin
                 Storage Object Admin
                 Storage Transfer Admin

3 - Pegar as credenciais (Terminal)
    gcloud config get-value project
    gcloud projects list
    gcloud config set project sign2go-prd

    $SERVICE_ACCOUNT_EMAIL="prd-business-bucket@sign2go-prd.iam.gserviceaccount.com"
    gcloud iam service-accounts keys create credentials-prd-business-bucket --iam-account $SERVICE_ACCOUNT_EMAIL

4 - Criar secret
    - editar o arquivo para este nome bucket-account.json
    kubectl create secret generic bucket-key -n sign2go-business-prd --from-file bucket-account.json

5 - Descomentar ou editar via Lens (Linhas 71 e 72 do arquivo de deployment) -  Não precisa

6 - Criar a variavel no bitbucket
    BUCKET_NAME

###
- kubectl create secret generic bucket-key -n sign2go-business-dev --from-file bucket-account.json