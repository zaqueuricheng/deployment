- gcloud config set project sign2go-hml

- kubectl create ns sign2go-business-hml
- kubectl apply -f db-secret-hml.yaml

# - Executar esses comando no linux, faça no ns padrão no ns onde ficará o deployment
- cat sign2go-dev-secret.json 
- kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat sign2go-dev-secret.json)" --docker-email="pull-image-gcr-kube@sign2go-dev.iam.gserviceaccount.com" -n sign2go-business-hml
- kubectl apply -f business-deploy-hml.yaml
- kubectl port-forward sign2go-business-56fb5d67f-wt8xb 80:80 -n sign2go-business-hml
- kubectl apply -f business-svc-hml.yaml
- kubectl port-forward sign2go-business-56fb5d67f-wt8xb 80:80 -n sign2go-business-hml
- kubectl apply -f business-ingress-hml.yaml
- Test: 
    hml-business.sign2go.com.br // Não carregará a pag. para resolver coloquei o IP e o DNS no arquivo host (C:\Windows\System32\drivers\etc)
- Test:
    hml-business.sign2go.com.br // Carregará mas vai solicitar o certificado resolve com a linha abaixo
- kubectl create secret tls sign2go-cert-tls --cert=star.sign2go.com.br.bundle.crt --key=star.sign2go.com.br.key -n sign2go-business-hml

# Swagger 

- https://dev-business.sign2go.com.br/doc
- https://hml-business.sign2go.com.br/doc
- https://business.sign2go.com.br/doc