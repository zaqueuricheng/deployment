############################
1 - Google Functions: 
	- https://cloud.google.com/functions/docs/quickstart
	- gcloud functions deploy helloGET --runtime nodejs10 --trigger-http --allow-unauthenticated

2 - API-Gateway:
	- https://cloud.google.com/api-gateway/docs/quickstart
	- gcloud api-gateway apis create demo-api --project=sign2go-dev
	- gcloud api-gateway apis describe demo-api --project=sign2go-dev

	- gcloud api-gateway api-configs create test-api-backend --api=api-backend --openapi-spec=openapi2-functions.yaml --project=sign2go-dev --backend-auth-service-account=sign2go-dev@appspot.gserviceaccount.com
	
	- gcloud api-gateway api-configs describe test-demo-api --api=demo-api --project=sign2go-dev

	- gcloud api-gateway gateways create gateway-api-demo --api=demo-api --api-config=test-demo-api --location=us-central1 --project=sign2go-dev

	- gcloud api-gateway gateways describe gateway-api-demo --location=us-central1 --project=sign2go-dev

3 - Test:
	- curl https://gateway-api-demo-dbcjdv64.uc.gateway.dev/hello