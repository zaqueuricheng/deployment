## Install
# Linux
- apt-get update
- apt-get install postgresql postgresql-contrib
- sudo lsof -i -P -n | grep LISTEN
- sudo netstat -tulpn | grep LISTEN
- ls /etc/init.d
- systemctl status postgresql
## - Acessando o postgres (SGBD)
- sudo -u postgres psql postgres
- \q # Sair do banco
- \password postgres # Mudar senha do usuário postgres
- CREATE EXTENSION adminpack; # Instala uma instensão para administração do postgres
- sudo -u postgres createuser -D -P zaqueur # Cria um usuário
- sudo -u postgres createuser --help
- sudo -u postgres createdb -O zaqueur dbTeste # Cria um banco de dados


## - 
create group desenvolvedores;
 
alter group desenvolvedores add user teste;
alter group desenvolvedores add user jonathanoliveira;
alter group desenvolvedores add user thiagocavaloti;
alter group desenvolvedores add user fabiovieira;
alter group desenvolvedores add user thiagosilva;
alter group desenvolvedores add user jribeiromodalgr;
 
 
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN schema public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN schema public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT USAGE ON SCHEMA public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;



## Dev
- psql --host=34.70.5.228 --port=5432 --username=postgres --dbname=sign2go-business-dev --password
- \l # Lista todos os bancos
- \q # Sair
- \du # Lista todos os usuários
- alter group desenvolvedores add user gbragamonte;
- \d # Lista relações
- alter table migrations owner to postgres

## Group
## Listar todos os usuários e grupos e manipulação
- \du
- https://www.postgresql.org/docs/8.0/sql-creategroup.html
- CREATE GROUP sign2fo_business_devs;
- CREATE GROUP sign2fo_business_devs WITH USER jonathan, david;
- DROP GROUP sign2fo_business_devs;
- ALTER GROUP sign2fo_business_devs ADD USER sign2go-business;
- ALTER GROUP desenvolvedores DROP USER gbragamonte;

## Privilégios aos usuários e grupos
- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO sign2fo_business_devs;
- GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO sign2fo_business_devs;
- GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO sign2fo_business_devs;
- GRANT USAGE ON SCHEMA public TO sign2fo_business_devs;

## Tabelas
ALTER TABLE public.migrations OWNER TO "sign2go-business";
GRANT ALL ON TABLE public.migrations TO gbragamonte;
GRANT ALL ON TABLE public.migrations TO "sign2go-business";

ALTER TABLE public.migrations OWNER TO "cloudsqlsuperuser";

## Prod
- pwd: 
- host: 
- psql --host=localhost --port=5432 --username=postgres --dbname=sign2go-business-prod --password

- ALTER TABLE public.migrations OWNER TO "cloudsqlsuperuser";
- ALTER GROUP validpgplugin ADD USER tcavaloti;
- ALTER GROUP sign2go-business ADD USER tcavaloti;

- Alterado permissão pelo SGBD 

## Development access
YUhvRE9nY0U1b25PQXhBMw==
psql --host=34.70.5.228 --port=5432 --username=cG9zdGdyZXM= --dbname=sign2go-business-dev --password

psql --host=34.70.5.228 --port=5432 --username=postgres --dbname=sign2go-business-dev --password 

\l  - lista todos os banco de dados
\q  - sair
\du - lista todos os usuários
\d  - lista grupos e relações

- ALTER GROUP gbragamonte ADD USER sign2go-business;
- ALTER GROUP sign2go-business ADD USER gbragamonte;

#####
create group desenvolvedores;
 
alter group desenvolvedores add user jhonathanbatista;
alter group desenvolvedores add user jonathanoliveira;
alter group desenvolvedores add user thiagocavaloti;
alter group desenvolvedores add user fabiovieira;
alter group desenvolvedores add user thiagosilva;
alter group desenvolvedores add user jribeiromodalgr;
 
 
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN schema public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN schema public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;
GRANT USAGE ON SCHEMA public, payments, sign_poc, sign_register, sign_signatures TO desenvolvedores;


####
psql --host=34.70.5.228 --port=5432 --username=postgres --dbname=sign2go-business-dev --password
aHoDOgcE5onOAxA3
\l
SELECT sign2go-business-dev FROM information_schema.tables WHERE table_schema='public'
SELECT sign2go-business-dev

\l - lista os bancos de dados
\d - lista as tabelas do banco selecionado
SELECT sign2go-business-dev


### Access privileges - Development
- create group business_dev;
\du - lista todos os grupos e usuários

- alter group business_dev add user postgres;
- alter group business_dev add user tcavaloti;
- alter group business_dev add user gbragamonte;
- alter group business_dev add user jribeiro;

\du - lista todos os usuários e grupos de membros

- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, payments TO business_dev;
- GRANT ALL PRIVILEGES ON ALL SEQUENCES IN schema public, payments TO business_dev;
- GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN schema public, payments TO business_dev;
- GRANT USAGE ON SCHEMA public, payments TO business_dev;

\l - Verificar se os privilegios foram dados

Testar se os usúarios têm permissão agora
Testado - Mas, os outros usuários não conseguem criar colunas ou apagar

##
\d - lista as tabelas do banco selecionado
- ALTER TABLE public.company OWNER TO "business_dev";
- ALTER TABLE public.company_member OWNER TO "business_dev";
- ALTER TABLE public.emailconfig OWNER TO "business_dev";
- ALTER TABLE public.emails OWNER TO "business_dev";
- ALTER TABLE public.histories OWNER TO "business_dev";
- ALTER TABLE public.login OWNER TO "business_dev";

- ALTER TABLE public.migrations OWNER TO "business_dev";
- ALTER TABLE public.migrations_id_seq OWNER TO "business_dev";
- ALTER TABLE public.participant_refusals OWNER TO "business_dev";
- ALTER TABLE public.participants OWNER TO "business_dev";

- ALTER TABLE public.participants_annotations OWNER TO "business_dev";
- ALTER TABLE public.scheduler OWNER TO "business_dev";
- ALTER TABLE public.useraccount OWNER TO "business_dev";

- ALTER SCHEMA public OWNER TO "business_dev";

Testar se os usúarios têm permissão agora
Testado - Mas, os outros usuários conseguem criar colunas ou apagar

## hml
- psql --host=35.247.241.161 --port=5432 --username=sign2go-business --dbname=sign2go-business-hml --password
- postgres
- \l - lista os bancos de dados
- \du - lista usuários e grupos
- create group business_dev;
- \du

##
- alter group business_dev add user postgres;
- alter group business_dev add user tcavaloti;
- alter group business_dev add user gbragamonte;
- alter group business_dev add user jribeiro;

## Deu erro porque não existem
- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public, payments TO business_dev;
- GRANT ALL PRIVILEGES ON ALL SEQUENCES IN schema public, payments TO business_dev;
- GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN schema public, payments TO business_dev;
- GRANT USAGE ON SCHEMA public, payments TO business_dev;

##
- ALTER TABLE public.migrations OWNER TO "business_dev";

### prod
- psql --host=35.247.247.205 --port=5432 --username=tcavaloti --dbname=sign2go-business-prod --password
%?V)zB:DW3$"/eo@-business_prodtc

- \l - lista os bancos de dados
- \du - lista usuários e grupos
- create group business_dev;
- \du

##
- alter group business_dev add user postgres;
- alter group business_dev add user tcavaloti;
- alter group business_dev add user gbragamonte;
- alter group business_dev add user jribeiro;

##
- psql --host=34.70.5.228 --port=5432 --username=tcavaloti --dbname=sign2go-business-dev --password
- ;N'%]l^Z,k9FjnL@-business_devtc

- psql --host=34.70.5.228 --port=5432 --username=gbragamonte --dbname=sign2go-business-dev --password
- qFFdNp[2x&g23b"@-business_devgb

- alter group gbragamonte add user postgres;
- alter group gbragamonte add user tcavaloti;
- alter group gbragamonte add user jribeiro;

##
## Dev users
- user: "gbragamonte" pwd: "qFFdNp[2x&g23b"@-business_devgb"
- user: "jribeiro" pwd: "[+J!P`.MN_)xcTW@-business_devjr"
- user: "tcavaloti" pwd: ";N'%]l^Z,k9FjnL@-business_devtc"
- ip: "34.70.5.228" port: "5432" db: "sign2go-business-dev"

## Hml users
- user: "gbragamonte" pwd: "-}A!$L|leC9S6yw@-business_hmlgb"
- user: "jribeiro" pwd: "7CqzUu]8@HXW#*Q@-business_hmljr"
- user: "tcavaloti" pwd: ")022?Y@.1u/}!X4@-business_hmltc"
- ip: "35.247.241.161" port: "5432" db: "sign2go-business-hml"

## Prod users
- user: "gbragamonte" pwd: ">N[-Z8wZdmYRMQ9@-business_prodgb"
- user: "jribeiro" pwd: "xq*nx,J*=YW';5*@-business_prodjr"
- user: "tcavaloti" pwd: "%?V)zB:DW3$"/eo@-business_prodtc"
- ip: "35.247.247.205" port: "5432" db: "sign2go-business-prod"

##
psql --host=34.70.5.228 --port=5432 --username=jribeiro --dbname=sign2go-business-dev --password
- alter group jribeiro add user postgres;
- alter group jribeiro add user tcavaloti;
- alter group jribeiro add user gbragamonte; // ERROR:  role "jribeiro" is a member of role "gbragamonte"

## 