# Install on GCP postgres

# https://randomkeygen.com/

## Dev - This instance was deleted
- instance id: psql-sign2go-business-develop
- pwd: psql-sign2go-business-develop

## DevOps user
- user: "sign2go-business" pwd: "s$9hwvoOm;!$k@8abcd1234-@"
- dbs: sign2go-business-dev, sign2go-business-hml, sign2go-business-prod

## Dev users
- user: "gbragamonte" pwd: "qFFdNp[2x&g23b"@-business_devgb"
- user: "jribeiro" pwd: "[+J!P`.MN_)xcTW@-business_devjr"
- user: "tcavaloti" pwd: ";N'%]l^Z,k9FjnL@-business_devtc"
- ip: "" port: "5432" db: "sign2go-business-dev"

## Hml users
- user: "gbragamonte" pwd: "-}A!$L|leC9S6yw@-business_hmlgb"
- user: "jribeiro" pwd: "7CqzUu]8@HXW#*Q@-business_hmljr"
- user: "tcavaloti" pwd: ")022?Y@.1u/}!X4@-business_hmltc"
- ip: "" port: "5432" db: "sign2go-business-hml"

## Prod users
- user: "gbragamonte" pwd: ">N[-Z8wZdmYRMQ9@-business_prodgb"
- user: "jribeiro" pwd: "xq*nx,J*=YW';5*@-business_prodjr"
- user: "tcavaloti" pwd: "%?V)zB:DW3$"/eo@-business_prodtc"
- ip: "" port: "5432" db: "sign2go-business-prod"

## Create databases
any
0.0.0.0/0