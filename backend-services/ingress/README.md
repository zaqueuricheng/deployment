- kubectl create namespace ingress
- kubectl create -f default-backend.yaml -n ingress

- kubectl create -f default-backend-service.yaml -n ingress
- kubectl create -f nginx-ingress-controller-config-map.yaml -n ingress

- kubectl get configmaps -n ingress

- kubectl create -f nginx-ingress-controller-service-account.yaml -n ingress

- kubectl create -f nginx-ingress-controller-clusterrole.yaml -n ingress

- kubectl create -f nginx-ingress-controller-clusterrolebinding.yaml -n ingress

- kubectl get serviceaccounts -n ingress
- kubectl get clusterrole -n ingress
- kubectl get clusterrolebindings -n ingress

- kubectl create -f nginx-ingress-controller-deployment.yaml -n ingress

- kubectl get deployments -n ingress

- kubectl create -f nginx-ingress.yaml -n ingress
- kubectl create -f nginx-ingress.yaml -n backend-svs

- kubectl get ingresses -n ingress
- kubectl get ingresses -n backend-svs

- kubectl describe ingresses.extensions nginx-ingress -n ingress