const express = require('express');
const app = express();

app.get('/demon', (req, res) => {
  // console.log('Hello world received a request.');

  // const target = process.env.TARGET || 'World';
  // res.send(`Hello ${target}!`);
  res.send("demon service");
});

const port = process.env.PORT || 81;
app.listen(port, () => {
  console.log('Hello world listening on port', port);
});