const express = require('express');
const app = express();

app.get('/foo', (req, res) => {
  // console.log('Hello world received a request.');

  // const target = process.env.TARGET || 'World';
  // res.send(`Hello ${target}!`);
  res.send("foo service");
});

const port = process.env.PORT || 82;
app.listen(port, () => {
  console.log('Hello world listening on port', port);
});