https://www.youtube.com/watch?v=DwlIn9zOcfc


https://stackoverflow.com/questions/35266769/how-to-list-the-published-container-images-in-the-google-container-registry-usin


## Steps
https://cloud.google.com/kubernetes-engine/docs/quickstarts/deploying-a-language-specific-app?hl=pt-br#standard


# 1 - Cria uma imagem
## 1.1 - Criar cluster e conectar ao cluster
- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

## 1.2 - Listar imagens do registry (Você pode ver isso no console do google também) - Container registry
- gcloud container images list

## 1.3 - Apagar imagens do registry (Você pode apagar pelo console também)
- gcloud container images delete gcr.io/monitoramento-valid/bar

## 1.3 - Criar imagens no cluster/registry
## Arquivos que devem ficar na mesma pasta para criar a imagem
### - package.json
### - index.js
### - Dockerfile
### - .dockerignore
- gcloud builds submit --tag gcr.io/monitoramento-valid/bar:0.0.3 . // Copy the image and past in deployment file...

# 2 - Criar namespace no cluster para organizar os deploy
- kubectl create ns backend-svs

# 3 - Criar o deploy da aplicação (Sobi o container com a imagem(Sua aplicação) no k8s)
- kubectl apply -f deploy-bar.yaml -n backend-svs

# 4 - Testar a aplicação
- kubectl port-forward bar-deployment-7ddd89b7cd-4j94n 80:80 -n backend-svs
- http://127.0.0.1:80/bar

# 5 - Criar o Service da aplicação (Service é um mecanismo que expõe sua aplicação dentro e fora do cluster)
- https://kubernetes.io/docs/concepts/services-networking/service/
## - ClusterIP - Expõe o serviçlo dentro do Cluster é o padrão
## - NodePort - 
## - LoadBalancer - Expõe o serviço externamente usando o balanceador de carga

## - Internal service (Cluster IP)
- kubectl apply -f svc-bar.yaml -n backend-svs
## - External service (Load balancer)
- kubectl apply -f svc-bar.yaml -n backend-svs

# - 6 - Ingress (É uma forma de expor vários serviços através de um unico IP)